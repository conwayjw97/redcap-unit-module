<?php
/**
 * Created by James Conway
 * User: conwayjw97
 * Date: 07/02/2019
 */
 
// Set the namespace defined in the config file
namespace TUDresden\REDCapUnitSupport;

use REDCap;

error_reporting(E_ALL);

// Include the standard REDCap header and sidebar
require_once APP_PATH_DOCROOT . 'ProjectGeneral/header.php';

// CSS Styling
echo "
<style>
	.h2-overwrite {
		font-family: 'Open Sans',Helvetica,Arial,sans-serif;
		font-size: 26px;
	}
		
	.td-overwrite {
		padding-bottom: 5px;
		font-family: 'Open Sans',Helvetica,Arial,sans-serif;
		font-size: 14px;
	}
	
	.hr-overwrite {
		margin: 0;
	}
	
	.root-overwrite {
		
		font-size: 16px;
	}
</style>
";

// JavaScript functions
echo "
	<script>
		var fieldToUnit = {};
		var reportID = '';
		
		function saveChoices(){
			for (var field in fieldToUnit) { 
				localStorage.setItem('".$module->getProjectId()." - ' + reportID + ' - ' + field, fieldToUnit[field]);
			}

		}
		
		function updateFieldToUnit(field){
			var elements = document.getElementsByTagName('select');
			for (i = 0; i < elements.length; i++) { 
				if(elements[i].name == field){
					fieldToUnit[field] = elements[i].value;
				}
			}
		}
		
		function updateReportID(id){
			reportID = id;
			console.log(id);
		}
	
		function selectFieldElement(field, unit){
			var elements = document.getElementsByTagName('select');
			for (i = 0; i < elements.length; i++) { 
				if(elements[i].name == field){
					elements[i].value = unit;
				}
			}
		};
	
		function selectElement(unit){
			var elements = document.getElementsByTagName('select');
			for (i = 0; i < elements.length; i++) {
				var options = elements[i].options;
				for (j = 0; j < options.length; j++) { 
					if(options[j].value.includes(unit)){
						elements[i].value = options[j].value;
					}
				}
			}
		};
		
		function selectMass(){    
			selectElement('g');
		};
		
		function selectAmount(){    
			selectElement('mol');
		};
	</script>";

// Get the project's instruments
$instruments = REDCap::getInstrumentNames();

// Get the string that indicates which fields are unit fields
$unitEndString = $module->getProjectSettings()['unit_end']['value'];

// Fetch JavaScript functions
echo "<script src='".$module->getUrl('javascript/math.js')."'></script>";
echo "<script src='".$module->getUrl('javascript/conversion.js')."'></script>";

// Begin post form for fields rendered below
echo "<form action='".$module->getUrl('REDCapReportExporter.php')."' method='post'>";

// If the user first arrives on the page
if($_SERVER['REQUEST_METHOD'] != "POST")
{
	$reports = \DataExport::getReportNames(null, true);
	$reportTitles = array_column($reports, 'title');
	$reportIDs = array_column($reports, 'report_id');
	
	echo "<h2 class='h2-overwrite' style='margin: 40px 0;'>
	Please select an existing report for this project.
	</h2><hr class='hr-overwrite'>";
	
	// Report ID dropdown and select button
	echo "<table style='margin: 40px 0;'>
		<tr>
			<td class='td-overwrite'>
				<h2 class='h2-overwrite' style='margin-right: 10px;'>Report: </h2>
			</td>
			<td class='td-overwrite'>
				<select name='report_id' style='margin-right: 10px;'>";
					foreach ($reportIDs as $index => $reportID) {
						echo "<option value='".$reportID."')'>(".$reportID.") ".$reportTitles[$index]."</option>";
					}
					
	echo "		</select>
			</td>
			<td class='td-overwrite'>
				<input type='submit' name='report' style='font-size: 100%;' value='Select Report'/>
			</td>
		</tr>
	</table>";
}

// If the user just selected a Report ID
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['report']))
{
	$report = REDCap::getReport($_POST['report_id'], 'csv', true);
	$rows = explode("\n", $report);
	$headers = explode(",", $rows[0]);
	
	echo "<script>updateReportID('".$_POST['report_id']."')</script>";
	
	echo "<h2 class='h2-overwrite' style='margin: 40px 0;'>
	Please select export units for each of the report's unit measurements.
	</h2><hr class='hr-overwrite'>";
	
	// Buttons to select common values for all fields
	echo "
		<span class='root-overwrite' style='padding-right:5px;'><h2 class='h2-overwrite'>Auto-Fill: </h2><br>(experimental feature)<br></span>
		<input type='button' value='Everything in amount of substance' style='margin-top:10px;' onclick='selectAmount();'/>
		<input type='button' value='Everything in mass' onclick='selectMass();'/>
		";

	// Iterate through the project's instruments and create a header for each one that contains fields that are in the report
	foreach ($instruments as $instrument=>$label) {
		$field_counter = 0;
		$fields = REDCap::getFieldNames($instrument);
		foreach ($fields as $unitName) {
			if (substr_compare( $unitName, $unitEndString, -strlen( $unitEndString ) ) === 0
					&&
					in_array($unitName, $headers)) $field_counter += 1;}
		if ($field_counter != 0){
			echo "<h2 class='h2-overwrite' style='margin: 40px 0;'>".$label."</h2><hr class='hr-overwrite'>";
			echo "<table style='margin: 40px;'>";}
		// Build the table of unit fields with corresponding units choice boxes for this instrument
		foreach ($fields as $unitName) {
			// Check if this string is a unit field, and is present in the report
			if (substr_compare( $unitName, $unitEndString, -strlen( $unitEndString ) ) === 0
					&&
					in_array($unitName, $headers)){
				$choices = $module->getChoiceLabels($unitName);
				echo "
					<tr>
						<td class='td-overwrite' style='min-width: 150px;'>".$unitName."</td>
						<td class='td-overwrite'>
							<select name='".$unitName."'  onchange=updateFieldToUnit('".$unitName."')>";
							// Add each of the corresponding unit choices to this unit's choice field
							foreach ($choices as $choice) {
								echo "<option value='".$choice."')'>".$choice."</option>";
							}
				echo "
							</select>
						</td>
					</tr>";
			}
		}
		echo "</table>";
	}
	
	// Check if this project has locally saved choices
	echo "<script>
	for (j = 0; j < localStorage.length; j++)   {
		var project = localStorage.key(j).split(' - ')[0];
		var repID = localStorage.key(j).split(' - ')[1];
		var field = localStorage.key(j).split(' - ')[2];
		var unit = localStorage.getItem(localStorage.key(j));
		if(project == ".$module->getProjectId()." && '".$_POST['report_id']."' == repID){
			selectFieldElement(field, unit);
		}
	}
	</script>";
	
	// CSV separator dropdown
	echo "
	<hr class='hr-overwrite'>
	<table style='margin-top: 40px; margin-bottom: 20px;'>
		<tr>
			<td class='td-overwrite'>
				<h2 class='h2-overwrite' style='margin-right: 10px;'>CSV Separator: </h2>
			</td>
			<td class='td-overwrite'>
				<select name='csv_separator'>
					<option value=',')'>Comma</option>
					<option value='\t')'>Tab</option>
					<option value=';')'>Semicolon</option>
				</select>
			</td>
		</tr>
	</table>";
	
	// Save button
	echo "<button name='save' style='margin-right: 10px;' onclick='saveChoices(); return false;'>Save unit choices for this report</button>";

	// Export button
	echo "<input type='submit' name='export' value='Create export'/>";
	
	// Report ID to pass to the next page
	echo "<input type='hidden' name='report_id' value='".$_POST['report_id']."'/>";
}

// If the user just pressed export
if($_SERVER['REQUEST_METHOD'] == "POST" and isset($_POST['export']))
{
	$report = REDCap::getReport($_POST['report_id'], 'csv', true);
	$rows = explode("\n", $report);
	$headers = explode(",", $rows[0]);
	
	// Put CSV report's contents into arrays
	echo "<script>var rowArray = [];</script>";
	for ($i = 0; $i < count($rows); $i++) {
		$cells = explode(",", $rows[$i]);
		echo "<script>var cellArray = [];</script>";
		for ($j = 0; $j < count($cells); $j++) {
			echo "<script>cellArray.push('".$cells[$j]."');</script>";
		}
		echo "<script>rowArray.push(cellArray);</script>";
	}
	
	//echo "<script>console.log(rowArray);</script>";
	
	
	// Collect the user's chosen molar masses and create corresponding units for them
	echo "<script>var molar_masses = new Array();</script>";
	foreach($module->getSubSettings('molars') as $molarMasses) {
		$compound = strtolower($molarMasses['compound']);
		$molar_mass = $molarMasses['molar_mass'];
		echo "<script>molar_masses['".$compound."'.toLowerCase()] = '".$molar_mass."';</script>";
	}
	
	//echo "<script>console.log(molar_masses);</script>";
		
		
	// Collect column indexes for units and their values
	$chosenValues = array_keys($_POST);
	$valueToIndex = array();
	for ($i = 0; $i < count($chosenValues); $i++) {
		// Check if this header is a unit field
		if (substr_compare( $chosenValues[$i], $unitEndString, -strlen( $unitEndString ) ) === 0){
			// Iterate through the headers to find the unit column and its corresponding value column
			$unitColumn = null;
			$valueColumn = null;
			for ($j = 0; $j < count($headers); $j++) {
				if($headers[$j] === $chosenValues[$i]){
					$unitColumn = $j;
				}
				if($headers[$j] === substr($chosenValues[$i], 0, -strlen( $unitEndString ))){
					$valueColumn = $j;
				}
			}
			if ($unitColumn !== null && $valueColumn !== null){
				$indices = array(
					"unitIndex" => $unitColumn,
					"valueIndex" => $valueColumn,
				);
				$valueToIndex[$chosenValues[$i]] = $indices;
			}
		}
	}
	
	//echo "<script>console.log('".var_dump($valueToIndex)."');</script>";
	
	
	// Convert cell values with math.js
	echo "<script>
	var headers = rowArray[0];
	var successFlag = true;
	</script>";
	for ($i = 0; $i < count($chosenValues); $i++) {
		echo "<script>
			if(headers.includes('".$chosenValues[$i]."')){
				var valueIndex = '".$valueToIndex[$chosenValues[$i]]['valueIndex']."';
				var unitIndex = '".$valueToIndex[$chosenValues[$i]]['unitIndex']."';
				var chosenUnit = '".$_POST[$chosenValues[$i]]."';
				var unitElement = '".$chosenValues[$i]."';
				var unitEndString = '".$unitEndString."';
				
				for(var i = 1; i < rowArray.length; i++){
					var row = rowArray[i];
					if(typeof row[valueIndex] !== 'undefined' && row[valueIndex] !== ''
						&& typeof row[unitIndex] !== 'undefined' && row[unitIndex] !== ''){
						try {
							var molarAlias = unitElement.substring(0, unitElement.length - unitEndString.length).toLowerCase();
							var convertedValue = convertUnit(row[unitIndex], chosenUnit, row[valueIndex], molar_masses, molarAlias);
							rowArray[i][valueIndex] = convertedValue;
							rowArray[i][unitIndex] = chosenUnit;
						}
						catch(err) {
							successFlag = false;
							rowArray[i][valueIndex] = err;
							rowArray[i][unitIndex] = chosenUnit;
						}
					}
				}
			}	
		</script>";
	}
	
	
	// CSV creator
	echo "<script>
		var csvContent = '';
		for(var i = 0; i < rowArray.length; i++){
			csvContent += rowArray[i].join('".$_POST['csv_separator']."') + '\\r\\n';
		}
	</script>";
	
	//echo "<script>console.log(csvContent);</script>";
	
	
	// Download CSV
	echo "<script>
		var element = document.createElement('a');
		element.setAttribute('href', 'data:csvContent/plain;charset=utf-8,' + encodeURIComponent(csvContent));
		element.setAttribute('download', 'report.csv');

		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	</script>";
	
	// Output the export result
	echo "
	<h2 class='h2-overwrite' style='padding-bottom: 40px;'>Export Created!</h2>
	<h2 class='h2-overwrite' style='padding-bottom: 40px;' id='exportResult'></h2>
	<script>
	if(successFlag == false){
		exportResult.innerText = 'Some unit values failed to convert properly, check the export to see which.';
	}
	</script>;
	";
}

// End post form
echo "</form>";
?>
