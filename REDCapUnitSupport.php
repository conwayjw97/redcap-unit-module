<?php
/**
 * Created by James Conway
 * User: conwayjw97
 * Date: 12/11/2018
 */
 
// Set the namespace defined in the config file
namespace TUDresden\REDCapUnitSupport;

use ExternalModules\AbstractExternalModule;
use REDCap;

error_reporting(E_ALL);

// Call the REDCap Connect file in the main "redcap" directory
require_once "../../redcap_connect.php";

// Module class
class REDCapUnitSupport extends AbstractExternalModule {

	function redcap_data_entry_form($project_id, $record, $instrument, $event_id, $group_id, $repeat_instance){
		// Get the string that indicates which fields are unit fields
		$unitEndString = $this->getProjectSetting('unit_end', $project_id);
		
		$this->addUnitConverter($instrument, $unitEndString);
		$this->addCalculatedFieldConverter($project_id, $record, $instrument, $event_id, $unitEndString);
	}

	function checkPubChem($molarAlias){
		  $curl = curl_init();
		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/{$molarAlias}/property/MolecularWeight/TXT",
		  CURLOPT_RETURNTRANSFER => true));

		$response = curl_exec($curl);
		$curlerr = curl_error($curl);
		curl_close($curl);
		if ($curlerr)
			exit("cURL Error #:" . REDCap::filterHtml($err));
		return floatval($response);
	}

	function redcap_module_save_configuration($project_id){
		$molarMasses = $this->getSubSettings('molars');
		foreach($molarMasses as &$molarMass) {
			if($molarMass['molar_mass'] == ""){
				$molarMass['molar_mass'] = (string) $this->checkPubChem($molarMass['compound']);
			}
		}
		$this->setProjectSetting('molar_mass', array_column($molarMasses, 'molar_mass'));
	}

	function addUnitConverter($instrument, $unitEndString){
		// Collect the user's chosen aliases and create corresponding units for them
		echo "<script src='".$this->getUrl('javascript/aliasing.js')."'></script>";
		foreach($this->getSubSettings('aliases') as $unitAliases) {
			$unit_aliase = $unitAliases['unit_aliase'];
			$aliased_unit = $unitAliases['aliased_unit'];
			echo "<script>createAlias('".$unit_aliase."', '".$aliased_unit."');</script>";
		}
		
		// Collect the user's chosen molar masses and create corresponding units for them
		echo "<script>var molar_masses = new Array();</script>";
		foreach($this->getSubSettings('molars') as $molarMasses) {
			$compound = strtolower($molarMasses['compound']);
			$molar_mass = $molarMasses['molar_mass'];
			echo "<script>molar_masses['".$compound."'.toLowerCase()] = '".$molar_mass."';</script>";
		}
		echo "<script>console.log('molar_masses', molar_masses);</script>";
		
		// Pass unit end string to JavaScript
		echo "
		<script>
			var unitEndString = '".$unitEndString."';
		</script>";
		
		// Fetch JavaScript functions
		echo "<script src='".$this->getUrl('javascript/math.js')."'></script>";
		echo "<script src='".$this->getUrl('javascript/conversion.js')."'></script>";
		echo "<script src='".$this->getUrl('javascript/listeners.js')."'></script>";
				
		// Iterate through all the fields
		$fields = REDCap::getFieldNames($instrument);
		foreach($fields as $unitName) {
			// Iterate through the unit field end strings
			$unitName = REDCap::filterHtml($unitName);

			// Check if this string is a unit field
			if (substr_compare( $unitName, $unitEndString, -strlen( $unitEndString ) ) === 0){
				$fieldName = REDCap::filterHtml(implode($unitEndString, explode($unitEndString, $unitName, -1)));
				
				// Pass field element names to JavaScript
				echo "
				<script>
					document.getElementsByName('".$unitName."')[0].addEventListener('click', function() {
						unitClick('".$unitName."', '".$fieldName."');
					});
					document.getElementsByName('".$unitName."')[0].addEventListener('change', function() {
						unitChange('".$unitName."', '".$fieldName."');
					});
				</script>";
			}
		}
	}

	function addCalculatedFieldConverter($project_id, $record, $instrument, $unitEndString){
		// Get the string that indicates which fields are unit fields
		$unitEndString = $this->getProjectSetting('unit_end', $project_id);
		
		echo"<script>
		var metadata_table = (status > 0 && page == 'Design/online_designer.php') ? 'metadata_temp' : 'metadata';
		$.get(app_path_webroot+'DataEntry/view_equation_popup.php', { pid: pid, field: 'calculated_field', metadata_table: metadata_table }, function(data) {
			console.log($('#viewEq').html(data));
		});
		</script>";
	}

}