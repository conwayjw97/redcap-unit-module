function findLongestWord(str) {
    const stringArray = str.split("_");
    const longestWord = stringArray.reduce((a, b) => {
        return (b.length > a.length) ? b : a;
    });
    return longestWord;
}

function checkPubChem(molarAlias) {
	var molarMass;
	// Check if a molar mass exists for this on pubchem
	$.ajax({
	   type: 'GET',
	   url: 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/' + molarAlias + '/property/MolecularWeight/TXT',
	   async: false,
        success: function (pubChemMass) {
            var reply = confirm('No molecular weight for ' + molarAlias + ' has been defined in the REDCap Unit Support plugin configuration. However, ' + parseFloat(pubChemMass) + ' g/mol was found on PubChem. Would you like to use it?');
			console.log('reply:', reply);
			if (reply == true) {
				molarMass = parseFloat(pubChemMass); 
			} else {
				molarMass = null;
			}
		}
	}).fail(function(){ 
		throw new Error('There is no molecular weight defined for ' + molarAlias + ' in the configuration of the REDCap Unit Support plugin or on PubChem.');
	});
	if(molarMass != null){
		return molarMass;
	}
	else{
		throw new Error('No molecular weight provided, cannot perform conversion.');
	}
}

function convertUnit(oldUnit, newUnit, fieldValue, molarMasses, molarAlias){
    var molarMass;
    molarAlias = findLongestWord(molarAlias);
	try {	
		// If the old unit is amount of substance and new unit is mass
		if(math.unit(oldUnit).equalBase(math.unit('mol')) 
			&& !math.unit(newUnit).equalBase(math.unit('mol')) 
			&& math.unit(newUnit).equalBase(math.unit('kg'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('mol').toNumber() * molarMass;
			return math.unit(conversion, 'gram').to(newUnit).toNumber();
		}
		
		// If the old unit is mass and new unit is amount of substance
		else if(math.unit(newUnit).equalBase(math.unit('mol')) 
			&& !math.unit(oldUnit).equalBase(math.unit('mol')) 
			&& math.unit(oldUnit).equalBase(math.unit('kg'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
				console.log(molarMass);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('g').toNumber() / molarMass;
			return math.unit(conversion, 'mol').to(newUnit).toNumber();
		}
		
		// If the old unit is amount/volume and new unit is mass/volume
		else if(math.unit(oldUnit).equalBase(math.unit('mol/l')) 
			&& !math.unit(newUnit).equalBase(math.unit('mol/l')) 
			&& math.unit(newUnit).equalBase(math.unit('kg/l'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('mol/l').toNumber() * molarMass;
			return math.unit(conversion, 'g/l').to(newUnit).toNumber();	
		}
		
		// If the old unit is mass/volume and new unit is amount/volume
		else if(math.unit(newUnit).equalBase(math.unit('mol/l')) 
			&& !math.unit(oldUnit).equalBase(math.unit('mol/l')) 
			&& math.unit(oldUnit).equalBase(math.unit('kg/l'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('g/l').toNumber() / molarMass;
			return math.unit(conversion, 'mol/l').to(newUnit).toNumber();	
		}
		
		// If the old unit is amount/time and new unit is mass/time
		else if(math.unit(oldUnit).equalBase(math.unit('mol/mins')) 
			&& !math.unit(newUnit).equalBase(math.unit('mol/mins')) 
			&& math.unit(newUnit).equalBase(math.unit('kg/mins'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('mol/mins').toNumber() * molarMass;
			return math.unit(conversion, 'g/mins').to(newUnit).toNumber();
		}
		
		// If the old unit is mass/time and new unit is amount/time
		else if(math.unit(newUnit).equalBase(math.unit('mol/mins')) 
			&& !math.unit(oldUnit).equalBase(math.unit('mol/mins')) 
			&& math.unit(oldUnit).equalBase(math.unit('kg/mins'))){
			// Check if a molar mass exists for this in the config
			if(molarAlias in molarMasses){
				molarMass = molarMasses[molarAlias];
			}
			else{
				// Check if a molar mass exists for this on pubchem
				molarMass = checkPubChem(molarAlias);
			}
			var conversion = math.unit(fieldValue, oldUnit).to('g/mins').toNumber() / molarMass;
			return math.unit(conversion, 'mol/mins').to(newUnit).toNumber();
		}
		
		// Otherwise, the conversion isn't related to molecular weightes
		else{
			return math.unit(fieldValue, oldUnit).to(newUnit).toNumber();	
		}
	}
	catch(err) {
		throw err;
	}
}