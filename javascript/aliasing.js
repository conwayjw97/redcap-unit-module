// Unit aliasing done in this file to prevent math.js import errors in main php file
function createAlias(unit_aliase, aliased_unit) {
	try{
		math.createUnit(unit_aliase, aliased_unit)
	}
	catch(err){
		console.log('\nFailed to create alias:', unit_aliase, 'for unit:', aliased_unit, '\n\n',err.message);
	}
}