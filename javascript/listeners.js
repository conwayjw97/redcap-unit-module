// Variables for the previously selected unit
var startUnitString;
var startUnitIndex;
var unitSelected;

console.log('molar_masses:', molar_masses);

// Whenever the user clicks on the unit dropdown for this field's unit
function unitClick(unitElement, fieldElement){
	// Get previously selected unit data
	var startUnit = document.getElementsByName(unitElement);
	startUnitString = '' + startUnit[0].options[startUnit[0].selectedIndex].text; 
	startUnitIndex = startUnit[0].value;
	
	// Check whether a unit was selected
	if(startUnitString != ''){
		unitSelected = true;
	}
	else{
		unitSelected = false;
	}
}

// Whenever the user changes the value for this field's unit
function unitChange(unitElement, fieldElement){
	// Get what's already in the field
	var field = document.getElementsByName(fieldElement);
	var fieldString =  '' + field[0].value; 
	// Get the unit that the user has selected
	var newUnit = document.getElementsByName(unitElement);
	var newUnitString = '' + newUnit[0].options[newUnit[0].selectedIndex].text; 
	
	// If a unit was selected before, try to convert the field's number to correspond to the new unit
	if(unitSelected && fieldString.trim() != '' && newUnitString != ''){
		try {
            var molarAlias = unitElement.substring(0, unitElement.length - unitEndString.length);
            conversion = convertUnit(startUnitString, newUnitString, fieldString, molar_masses, molarAlias);
			console.log('conversion: ', conversion);
			field[0].value = conversion;
		}
		catch(err) {
			window.alert(err);
			newUnit[0].value = startUnitIndex;
		}
	}
}